package com.chanllengef.employees;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Employee {

	private long id;
	private String employee_name;
	private long employee_salary;
	private long employee_age;
	private String profile_image;
	
	public Employee () {
	}

	public long getId() {
		return id;
	}

	public String getEmployee_name() {
		return employee_name;
	}

	public long getEmployee_salary() {
		return employee_salary;
	}

	public long getEmployee_age() {
		return employee_age;
	}

	public String getProfile_image() {
		return profile_image;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setEmployee_name(String employee_name) {
		this.employee_name = employee_name;
	}

	public void setEmployee_salary(long employee_salary) {
		this.employee_salary = employee_salary;
	}

	public void setEmployee_age(long employee_age) {
		this.employee_age = employee_age;
	}

	public void setProfile_image(String profile_image) {
		this.profile_image = profile_image;
	}
}