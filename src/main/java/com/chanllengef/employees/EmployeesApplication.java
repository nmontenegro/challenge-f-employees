package com.chanllengef.employees;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.Identity;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

@SpringBootApplication
@RestController
public class EmployeesApplication {

	public static void main(final String[] args) {
		SpringApplication.run(EmployeesApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate(final RestTemplateBuilder builder) {
		return builder.build();
	}

	@GetMapping("/")
	public String hello() {
		return "OK";
	}

	@PostMapping(value = "/employee", consumes = "application/json")
	public String employee(final RestTemplate restTemplate, CsvMapper csvMapper) {
		csvMapper = new CsvMapper();

		final EmployeeWrapper employees = restTemplate.getForObject(
			"http://dummy.restapiexample.com/api/v1/employees", EmployeeWrapper.class);

		String csv_string = "";

		try {
			final CsvSchema schema = csvMapper.schemaFor(Employee.class).withHeader();
			csv_string = csvMapper.writer(schema).writeValueAsString(employees.getData());
		} catch (JsonProcessingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			ChannelSftp channelSftp;
			channelSftp = setupJsch();
			channelSftp.connect();
	  
			InputStream stream = new ByteArrayInputStream(csv_string.getBytes(StandardCharsets.UTF_8));

			String remoteDir = "/home/nmontenegro.challenges/remote_sftp_test/";
		
			channelSftp.put(stream, remoteDir + "test_file.csv");
		
			channelSftp.exit();
		} catch (JSchException | SftpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return csv_string;
	}


	private ChannelSftp setupJsch() throws JSchException {
		final JSch jsch = new JSch();
		jsch.setKnownHosts("./ssh_key/known_hosts");
		jsch.addIdentity("./ssh_key/sftp-server");
		final Session jschSession = jsch.getSession("nmontenegro.challenges", "104.198.198.103");
		jschSession.connect();
		return (ChannelSftp) jschSession.openChannel("sftp");
	}
}
