FROM maven:3.6.3-jdk-11 AS builder
WORKDIR /code
COPY ./.mvn /code/.mvn
COPY ./src /code/src
COPY ./pom.xml /code/pom.xml
RUN mvn dependency:resolve
RUN mvn package -PdockerBuildDir

FROM mcr.microsoft.com/java/jre:11u7-zulu-alpine AS runtime
WORKDIR /app
COPY --from=builder /code/output /app
COPY ./entrypoint.sh /app

ENTRYPOINT ["sh", "/app/entrypoint.sh"]
